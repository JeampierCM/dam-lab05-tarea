import md5 from "md5";

const ts = Date.now();
const privateKey='e6c9e502682770b7897994e35a3708f411b24679'
const publicKey= '2d9ef3f3d5cc5f6964c60b0a276cffb9'
const hash = md5(`${ts}${privateKey}${publicKey}`);
const apiParams = {
  ts,
  apikey: publicKey,
  hash,
  baseURL: "https://gateway.marvel.com",
};
export default apiParams;

