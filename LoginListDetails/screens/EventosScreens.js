import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image ,SafeAreaView,ScrollView,FlatList,TouchableOpacity } from "react-native";
import apiParams from "../utils/helpers/marvel";
import { useNavigation } from "@react-navigation/native";
import InfoEventosScreens from "./InfoEventosScreens"

const   EventosScreens = ({route}) => {

    const [events, setEvents] = useState([]);
    const [selectedEvent, setSelectedEvent] = useState(null);

    useEffect(() => {
        const fetchEvents = async () => {
        const response = await fetch(
            `${apiParams.baseURL}/v1/public/events?ts=${apiParams.ts}&apikey=${apiParams.apikey}&hash=${apiParams.hash}`
        );
        const data = await response.json();
        setEvents(data.data.results);
        };
        fetchEvents();
    }, []);
    
	const Item = ({ title, description, image }) => (
		<TouchableOpacity onPress={() => setSelectedEvent({title, description, image})}>
			<View style={styles.item}>
				<Image style={styles.image} source={{ uri: `${image}` }} />
				<View style={styles.textContainer}>
					<Text style={styles.title}>{title}</Text>
					<Text numberOfLines={3} style={styles.description}>{description}</Text>
				</View>
			</View>
		</TouchableOpacity>
	);

	const renderItem = ({ item }) => (
	<Item title={item.title} description={item.description} image={`${item.thumbnail.path}.${item.thumbnail.extension}`} />
	);

  const navigation = useNavigation();
		if (selectedEvent) {
			navigation.navigate("EventoInfo", { item: selectedEvent });
			setSelectedEvent(null);
      console.log("Se envio el item seleccionado a la nueva pantalla");
		}

	return (
		<SafeAreaView style={styles.container}>
			<FlatList

				data={events}
				renderItem={renderItem}
				keyExtractor={item => item.id}
			/>
			{selectedEvent && (
				<InfoEventosScreens
					title={selectedEvent.title}
					description={selectedEvent.description}
					image={`${selectedEvent.image}`}
          cosolog={console.log(selectedEvent)}
				/>
			)}
		</SafeAreaView>
	);
	
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 20,
	},
	item: {
		backgroundColor: "#88FCF6",
		padding: 10,
		margin: 10,
		flexDirection: "row",
		borderRadius: 30,
    height:150,
	},
	title: {
		fontSize: 22,
		fontWeight: "bold",
	},
	description: {
		fontSize: 16,
	},
	image: {
		width: 100,
		height: 100,
		borderRadius: 30,
		objectFit: "fill",
    	padding: 10,
	},
	textContainer: {
		flex: 1,
		flexDirection: "column",
		marginLeft: 10,
    justifyContent: "center",
		textAlign: "center",
	},
});

export default EventosScreens;