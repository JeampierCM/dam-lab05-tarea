import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";


const InfoEventosScreens = ({ route }) => {

	const { item } = route.params;
	return (
		<View style={styles.container}>
			<Image style={styles.image} source={{ uri: item.image }} />
			<Text style={styles.title}>{item.title}</Text>
			<Text style={styles.description}>{item.description}</Text>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		marginTop: 20,
        alignItems: "center",
	},
	title: {
		fontSize: 22,
		fontWeight: "bold",
	},
	description: {
		fontSize: 16,
	},
	image: {
		width: 350,
		height: 350,
		borderRadius: 30,
		objectFit: "fill",
		padding: 10,
	},
});

export default InfoEventosScreens;

