import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

//Icons
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";

//screens
import EventosScreens from "./screens/EventosScreens"
import LoginScreens from "./screens/LoginScreens"
import InfoEventosScreens from "./screens/InfoEventosScreens"

//screens Stack Login

const LoginStackNavigator = createNativeStackNavigator();
const LoginStack = () => {
	return (
		<LoginStackNavigator.Navigator initialRouteName="Login">
			<LoginStackNavigator.Screen name="Login" component={LoginScreens} />
			<LoginStackNavigator.Screen name="Eventos" component={EventosScreens} />
			<LoginStackNavigator.Screen name="EventoInfo" component={InfoEventosScreens} />
		</LoginStackNavigator.Navigator>
	);
}

const Navegation = () => {
	return (
		<NavigationContainer>
			<LoginStack />
		</NavigationContainer>
	);
}

export default Navegation;

